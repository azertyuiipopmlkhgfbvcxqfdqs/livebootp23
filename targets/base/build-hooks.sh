
target_docker_build_before() {
    local pattern='^.*-ubuntu-.*$'
    if [[ $LIBEBOOTP_TARGET_IMAGE_VERSION =~ $pattern ]]; then
        local from_ubuntu_version=${LIBEBOOTP_TARGET_IMAGE_VERSION##*-ubuntu-}
        LIBEBOOTP_DOCKER_BUILD_ARGS+=(--build-arg "FROM_UBUNTU_VERSION=$from_ubuntu_version")
        print_note "Image based on Ubuntu docker image version $from_ubuntu_version"
    fi
}
